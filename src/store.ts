import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    languages: [
      {
        'key': 'es',
        'Desc': 'Español'
      },
      {
        'key': 'en',
        'Desc': 'English'
      }
    ],
    language: localStorage.getItem('language') ? JSON.parse('' + localStorage.getItem('language')) : 'en'
  },
  getters: {
    languages (state:any) : Array<any> {
      return state.languages
    },
    language (state:any) : string {
      return state.language
    }
  },
  mutations: {
    CHANGE_LANG: (state:any, language:any) => {
      state.language = language
      localStorage.setItem('language', JSON.stringify(language))
      window.location.reload()
    }
  }
})
