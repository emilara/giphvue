'use scrict'
import axios from 'axios'
import config from './../config'
import store from './store'

function sPath (path:any, param1?:any, param2?:any) {
  let lang = store.state.language

  switch (path) {
    case 'searcher' : return `search?q=${param1}&lang=${lang}&api_key=`
    case 'searcher_limit' : return `search?q=${param1}&lang=${lang}&limit=${param2}&api_key=`
    case 'giph' : return `${param1}?&lang=${lang}&api_key=`
    case 'trending' : return `trending?&lang=${lang}&limit=${param1}&api_key=`
    case 'searcher_update': return `search?q=${param1}&offset=${param2}&lang=${lang}&limit=${param2}&api_key=`
    case 'trending_update': return `trending?&offset=${param1}&lang=${lang}&limit=${param1}&api_key=`
  }
  return '/not-found'
}

let methods = {
  async link (path:any, param1?:any, param2?:any): Promise<any|null> {
    let sUrl = sPath(path, param1, param2)
    let url = config.general.services.url + sUrl + config.general.services.key
    try {
      let resp = await axios.get(url)
      return resp
    } catch (error) {
      console.error('Service error:', error.message)
      return null
    }
  }
}

export default methods
