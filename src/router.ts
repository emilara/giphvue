import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/:id?',
      name: 'home',
      component: Home
    },
    {
      path: '/giph/:id?',
      name: 'giph',
      component: () => import('./views/Giph.vue')
    }
  ]
})
